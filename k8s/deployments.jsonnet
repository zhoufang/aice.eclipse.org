local deployment = import "../../releng/hugo-websites/kube-deployment.jsonnet";

deployment.newProductionDeploymentWithStaging(
  "aice.eclipse.org", "aice-staging.eclipse.org"
)